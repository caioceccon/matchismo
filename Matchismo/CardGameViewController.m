//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Caio Ceccon on 6/12/13.
//  Copyright (c) 2013 Caio Ceccon. All rights reserved.
//

#import "CardGameViewController.h"

@interface CardGameViewController ()

@end

@implementation CardGameViewController
- (IBAction)flipCard:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
}

@end
